//
//  MData.swift
//  MData
//
//  Created by Matt on 11/6/15.
//  Copyright © 2015 Digital Eye LLC. All rights reserved.
//
//  This is mostly the AEStack class from the AERecord project, with some bits removed
//  https://github.com/tadija/AERecord

//
// AERecord.swift
//
// Copyright (c) 2014 Marko Tadić <tadija@me.com> http://tadija.net
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

import Foundation
import CoreData

public class MData {
    public class var sharedInstance: MData {
        struct Static {
            static let instance: MData = MData()
        }
        return Static.instance
    }
    
    public class var bundleIdentifier: String {
        let bundle = Bundle(for: self)
        return bundle.bundleIdentifier!
    }
    public class var defaultURL: URL {
        return storeURLForName(name: bundleIdentifier)
    }
    public class var defaultModel: NSManagedObjectModel {
        return NSManagedObjectModel.mergedModel(from: nil)!
    }
    
    public var managedObjectModel: NSManagedObjectModel?
    public var persistentStoreCoordinator: NSPersistentStoreCoordinator?
    public var mainContext: NSManagedObjectContext!
    public var backgroundContext: NSManagedObjectContext!
    dynamic public var defaultContext: NSManagedObjectContext {
        if Thread.isMainThread {
            return mainContext
        } else {
            return backgroundContext
        }
    }
    
    public class func storeURLForName(name: String) -> URL {
        let applicationDocumentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last!
        let storeName = "\(name).sqlite"
        return applicationDocumentsDirectory.appendingPathComponent(storeName)
    }
    
    public class func modelFromBundle(forClass: AnyClass) -> NSManagedObjectModel {
        let bundle = Bundle(for: forClass)
        return NSManagedObjectModel.mergedModel(from: [bundle])!
    }
    
    public func loadCoreDataStack(
        managedObjectModel: NSManagedObjectModel = defaultModel,
        storeType: String = NSSQLiteStoreType,
        configuration: String? = nil,
        storeURL: URL = defaultURL,
        options: [NSObject : AnyObject]? = nil) throws {
            
            self.managedObjectModel = managedObjectModel
            
            // setup main and background contexts
            mainContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
            backgroundContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
            
            // create the coordinator and store
            persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
            if let coordinator = persistentStoreCoordinator {
                try coordinator.addPersistentStore(ofType: storeType, configurationName: configuration, at: storeURL, options: options)
                mainContext.persistentStoreCoordinator = coordinator
                backgroundContext.persistentStoreCoordinator = coordinator
                startReceivingContextNotifications()
            }
    }
    
    public func destroyCoreDataStack(storeURL: URL = defaultURL) throws {
        // must load this core data stack first
        do {
            try loadCoreDataStack(storeURL: storeURL) // because there is no persistentStoreCoordinator if destroyCoreDataStack is called before loadCoreDataStack
            // also if we're in other stack currently that persistentStoreCoordinator doesn't know about this storeURL
        } catch {
            throw error
        }
        stopReceivingContextNotifications() // stop receiving notifications for these contexts
        
        // reset contexts
        mainContext.reset()
        backgroundContext.reset()
        
        // finally, remove persistent store
        if let coordinator = persistentStoreCoordinator {
            if let store = coordinator.persistentStore(for: storeURL) {
                try coordinator.remove(store)
                try FileManager.default.removeItem(at: storeURL)
            }
        }
        
        // reset coordinator and model
        persistentStoreCoordinator = nil
        managedObjectModel = nil
    }
    
    deinit {
        stopReceivingContextNotifications()
    }
    
    // MARK: Context Save
    
    public func saveContext(context: NSManagedObjectContext? = nil) {
        let moc = context ?? defaultContext
        moc.perform { () -> Void in
            if moc.hasChanges {
                do {
                    try moc.save()
                } catch {
                    print(error)
                }
            }
        }
    }
    
    public func saveContextAndWait(context: NSManagedObjectContext? = nil) {
        let moc = context ?? defaultContext
        moc.performAndWait { () -> Void in
            if moc.hasChanges {
                do {
                    try moc.save()
                } catch {
                    print(error)
                }
            }
        }
    }
    
    public func startReceivingContextNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(MData.contextDidSave(notification:)), name: NSNotification.Name.NSManagedObjectContextDidSave, object: mainContext)
        NotificationCenter.default.addObserver(self, selector: #selector(MData.contextDidSave(notification:)), name: NSNotification.Name.NSManagedObjectContextDidSave, object: backgroundContext)
    }
    
    public func stopReceivingContextNotifications() {
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc public func contextDidSave(notification: Notification) {
        if let context = notification.object as? NSManagedObjectContext {
            let contextToRefresh = context == mainContext ? backgroundContext : mainContext
            contextToRefresh?.perform({ () -> Void in
                contextToRefresh?.mergeChanges(fromContextDidSave: notification)
            })
        }
    }
    
    public class func refreshObjects(objectIDS: [NSManagedObjectID], mergeChanges: Bool, context: NSManagedObjectContext = MData.sharedInstance.defaultContext) {
        for objectID in objectIDS {
            context.performAndWait { () -> Void in
                do {
                    let managedObject = try context.existingObject(with: objectID)
                    // turn managed object into fault
                    context.refresh(managedObject, mergeChanges: mergeChanges)
                }
                catch {
                    print(error)
                }
            }
        }
    }
    
    public class func refreshAllRegisteredObjects(mergeChanges: Bool, context: NSManagedObjectContext = MData.sharedInstance.defaultContext) {
        var registeredObjectIDS = [NSManagedObjectID]()
        for object in context.registeredObjects {
            registeredObjectIDS.append(object.objectID)
        }
        refreshObjects(objectIDS: registeredObjectIDS, mergeChanges: mergeChanges)
    }
    
}
