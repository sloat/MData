//
//  MDataModel.swift
//  MData
//
//  Created by Matt on 11/6/15.
//  Copyright © 2015 Digital Eye LLC. All rights reserved.
//
//  This is inspired by the AERecord class
//  https://github.com/tadija/AERecord

//
// AERecord.swift
//
// Copyright (c) 2014 Marko Tadić <tadija@me.com> http://tadija.net
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.
//

import Foundation
import CoreData

public class MDataModel<T: NSManagedObject> {
    
    public class var entity_name: String {
        let class_name = "\(T.self)"
        return class_name.components(separatedBy: ".").last!
    }
    
    public class var current_key: String {
        var class_name = "\(T.self)"
        class_name = class_name.components(separatedBy: ".").last!
        return "current_\(class_name.lowercased())"
    }
    
    public class func fetchRequest() -> NSFetchRequest<NSFetchRequestResult> {
        return NSFetchRequest(entityName: entity_name)
    }
    
    public class func countAll() -> Int? {
        let req = MDataModel.fetchRequest()
        let ctx = MData.sharedInstance.defaultContext
        
//        let count = ctx.countForFetchRequest(req, error: &error)
        
        do {
            let count = try ctx.count(for: req)
            return count
        }
        catch _ {
            return nil
        }
        
    }
    
    public class func current() -> T? {
        let settings = UserDefaults.standard
        let ctx = MData.sharedInstance.defaultContext
        
        if let current_id = settings.url(forKey: current_key) {
            if let objID = MData.sharedInstance.persistentStoreCoordinator?.managedObjectID(forURIRepresentation: current_id) {
                return ctx.object(with: objID) as? T
            }
        }
        
        return nil
    }
    
    public class func insert() -> T? {
        let ctx = MData.sharedInstance.defaultContext
        let ename = entity_name
        
        if let entity = NSEntityDescription.entity(forEntityName: ename, in: ctx) {
            let obj = NSManagedObject(entity: entity, insertInto: ctx) as! T
            
            obj.set_current()
            return obj
        }
        
        return nil
    }
    
    public class func executeFetchRequest(fetchRequest: NSFetchRequest<NSFetchRequestResult>) throws -> [T] {
        let ctx = MData.sharedInstance.defaultContext
        
        return try ctx.fetch(fetchRequest) as! [T]
    }
}

extension NSManagedObject {
    
    public var entity_name: String {
        let obj: NSManagedObject = self
        let classname = "\(type(of: obj).self)"
//        return classname.componentsSeparatedByString(".").last!
        return classname.components(separatedBy: ".").last!
    }
    public var current_key: String {
        return "current_\(self.entity_name)".lowercased()
    }
    
    public func rollback() {
        MData.sharedInstance.defaultContext.refresh(self, mergeChanges: false)
    }
    
    public func save() {
        let ctx = MData.sharedInstance.defaultContext
        if ctx.hasChanges {
            do {
                try ctx.save()
            }
            catch let error {
                print("Error saving \(entity_name): \(error)")
                return
            }
            
            self.set_current()
        }
    }
    
    public func delete() {
        let ctx = MData.sharedInstance.defaultContext
        
        ctx.delete(self)
        do {
            try ctx.save()
        }
        catch let error {
            print("Error deleting \(entity_name): \(error)")
        }
    }
    
    public func set_current() {
        let settings = UserDefaults.standard
        let uri = self.objectID.uriRepresentation()
        
        settings.set(uri, forKey: current_key)
        settings.synchronize()
    }
    
}
