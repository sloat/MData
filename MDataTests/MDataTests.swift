//
//  MDataTests.swift
//  MDataTests
//
//  Created by Matt on 11/6/15.
//  Copyright © 2015 Digital Eye LLC. All rights reserved.
//

import XCTest
import CoreData
@testable import MData

typealias MProfile = MDataModel<Profile>

class MDataTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        let model = MData.modelFromBundle(forClass: MDataTests.self)
        do {
            try MData.sharedInstance.loadCoreDataStack(managedObjectModel: model, storeType: NSInMemoryStoreType)
        } catch let error {
            print("Error loading stack: \(error)")
        }
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        
        do {
            try MData.sharedInstance.destroyCoreDataStack()
        } catch let error {
            print("Error dismantling stack: \(error)")
        }
    }
    
    func testInsert() {
        let profile = MProfile.insert()
        
        XCTAssertNotNil(profile, "profile should be a newly inserted object")
    }
    
    func testCurrent() {
        let profile = MProfile.insert()
        profile?.name = "xyz"
        profile?.save()
        
        let p = MProfile.current()
        
        XCTAssertTrue(p?.name == "xyz", "current profile name not equal to old profile name. \(p?.name)")
    }
    
    func testDelete() {
        let profile = MProfile.current()
        profile?.delete()
        
        let p = MProfile.current()
        XCTAssertNil(p, "current profile should be nil")
    }
    
    func testFetch() {
        let p1 = MProfile.insert()
        p1?.name = "one"
        p1?.save()
        
        let p2 = MProfile.insert()
        p2?.name = "two"
        p2?.save()
        
        let req = MProfile.fetchRequest()
        
        var res: [Profile]
        do {
            res = try MProfile.executeFetchRequest(fetchRequest: req)
            let p = res.first
            XCTAssertTrue(p?.name == "one" || p?.name == "two", "didn't find any results")
        }
        catch let error {
            XCTFail("Error executing fetch: \(error)")
        }
    }
}
