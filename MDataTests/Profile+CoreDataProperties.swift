//
//  Profile+CoreDataProperties.swift
//  MData
//
//  Created by Matt on 11/6/15.
//  Copyright © 2015 Digital Eye LLC. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

import Foundation
import CoreData

extension Profile {

    @NSManaged var name: String?
    @NSManaged var created: NSDate?

}
