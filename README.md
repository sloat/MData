MData
=====

MData is an offshoot of [AERecord](https://github.com/tadija/AERecord) that 
implements classes using generics for Swift. The CoreData stack initialization 
is largely unchanged from AERecord, so you can count on MData to set up main and
background contexts, persistent stores, etc.


Installation
------------

1. Clone the repo
2. Reference MData project from another project
3. Do it several more times because XCode probably crashed


Initialization
--------------

In your AppDelegate's `didFinishLaunchingWithOptions` method:
```swift
do {
    try MData.sharedInstance.loadCoreDataStack()
}
catch let error {
    print("Core data failed to load: \(error)")
}
```

Don't forget to `import MData` in your code. And you'll probably want to save on
`applicationWillTerminate` with `MData.sharedInstance.saveContext()`.


Setting up models
-----------------

The easiest thing to do is to keep your Models inheriting from NSManagedObject
and wrap them with MDataModel. E.g.:

```swift
class Profile: NSManagedObject {

    @NSManaged var special_id: String
    @NSManaged var name: String
    @NSManaged var email: String
    
}

//Add a type alias to keep things neater.
typealias MProfile = MDataModel<Profile>
```

You can easily extend either the Profile class for instance methods and properties, or
the MDataModel class for additional instantiation methods :

```swift
extension Profile {
    func getEmailHostname() -> String? {
        //...
    }
}

extension MDataModel where T: Profile {
    class func findBySpecialID(specialIDs: Array<String>) -> Array<Profile>? {
        //...
    }
}
```


Using Models
------------

MDataModel provides methods for creating, inserting, and querying objects,
as well as tracking the last object edited or created.

MDataModel also extends NSManagedObject to provide convenience methods on
managed object instances.

Note that you don't initialize MDataModel objects. MDataModel classes give you
instantiated objects of the specific type. 

```swift
// (strict typing is not necessary here)
var p: Profile = MProfile.insert()

p.special_id = "special1"
p.name = "Some name"
p.save()
```


MDataModel class methods
------------------------

```swift
fetchRequest() -> NSFetchRequest
    //Create a fetch request object initialized with the core data entity

countAll() -> Int?
    //Count all managed objects, nil on error

current() -> T?
    //Return the last saved or created managed object of type T. nil if the object
    //does not exist. See also 'NSManagedObject.set_current()' extension method.

insert() -> T? - 
    //Create an object and insert it in to the current context. Set the new object as
    //current. Returns nil if the entity can't be found.

executeFetchRequest(fetchRequest: NSFetchRequest) throws -> [T] 
    //Convenience method to use the current context to execute an NSFetchRequest
    //object. Returns an array of objects or rethrows the error from the context
    //executeFetchRequest method.
```


NSManagedObject instance extensions
-----------------------------------

```swift

rollback()
    //Reverts changes to instance. Wrapper over current contexts refreshObject
    //method.

save()
    //Commits changes to an instance and sets the instance as the current object.
    //Does not do anything if the current context hasChanges property is false.

delete()
    //Deletes the instance and commits the change.

set_current()
    //Sets the instance to be the current object, as returned by
    //MDataModel<T>.current()
```


TODO
----

* Add some examples
* Write more unit tests if necessary 
* ???
